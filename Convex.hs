{-# LANGUAGE BangPatterns #-}
{-# OPTIONS_GHC -fno-warn-tabs #-}
import Control.Arrow
import Data.Maybe
import Data.List
import qualified Data.Map as M
import Data.Ratio
import Debug.Trace
import System.IO
import Util
import Control.Applicative ((<$>))

splitPolygon :: Polygon -> Edge -> [Polygon]
splitPolygon ps e = search1 (pairs ps)
	where
		search1 ((p, p'):ps) = if online e p
			then case search2 ps of
				r1:r2:rs -> (p:r1):(p:r2):rs
				[r] -> [p:r]
			else let s = E p p'; i = intersection s e in
				if isJust i && inRange s (fromJust i) && fromJust i /= p'
					then case search2 ps of
						r1:r2:rs -> (p:fromJust i:r1):(fromJust i:r2):rs
					else case search1 ps of
						r:rs -> (p:r):rs
		search1 [] = [[]]
		search2 ((p, p'):ps) = if online e p
			then [p:map fst ps, [p]]
			else let s = E p p'; i = intersection s e in
				if isJust i && inRange s (fromJust i) && fromJust i /= p'
					then [fromJust i:map fst ps, [p, fromJust i]]
					else case search2 ps of
						r1:r2:rs -> r1:(p:r2):rs
						[r] -> [p:r]
		search2 [] = [[]]

isRight :: Edge -> Polygon -> Bool
isRight (E a b) ps = all (clockwise a b) ps

data PolyFold = PolyFold { polygon :: Polygon, affine :: Affine } deriving (Show)

foldRight :: PolyFold -> Edge -> [PolyFold]
foldRight (PolyFold p a) e = map (\p -> if isRight e p then PolyFold p a else PolyFold (map (mirror e) p) $ mirrorAffine e a) $ filter (\p -> length p > 2) $ splitPolygon p e

foldConvexCW :: Polygon -> [PolyFold] -> [PolyFold]
foldConvexCW ps = go (cycle pps)
	where
		pps = pairs ps
		isRightPoly fs = all (\(a, b) -> all (isRight (E a b)) (map polygon fs)) pps
		go ((p, p'):ps) fs
			| isRightPoly fs = fs
			| otherwise = go ps $ fs >>= (`foldRight` E p p')
		go [] fs = fs

assignNames :: Ord a => [a] -> (M.Map a Int, M.Map Int a)
assignNames = go 0 M.empty M.empty
	where
		go !n !m !im [] = (m, im)
		go n m im (x:xs) = if M.member x m
			then go n m im xs
			else go (n + 1) (M.insert x n m) (M.insert n x im) xs

showSolution :: [PolyFold] -> String
showSolution ps = show (M.size points) ++ "\n"
		++ intercalate "\n" (map (showPos . snd) $ M.toAscList points) ++ "\n"
		++ show (length ps) ++ "\n"
		++ intercalate "\n" (map showFacet unfolds) ++ "\n"
		++ intercalate "\n" (map (showPos . (mapping M.!) . snd) $ M.toAscList points)
	where
		(names, points) = assignNames $ concat unfolds
		mapping = M.fromList $ ps >>= (\(PolyFold p a) -> map (unapplyAffine a &&& id) p)
		unfolds = map (\(PolyFold p a) -> map (unapplyAffine a) p) ps
		showPos (x,y) = showFrac x ++ "," ++ showFrac y
		showFrac a = if denominator a == 1 
			then show (numerator a)
			else show (numerator a) ++ "/" ++ show (denominator a)
		showFacet ps = show (length ps) ++ " " ++ intercalate " " (map show $ map (names M.!) ps)

boundSquare :: Polygon -> Coord
boundSquare ps = max width height
	where
		width = maximum (map fst ps) - minimum (map fst ps)
		height = maximum (map snd ps) - minimum (map snd ps)

turnToFit :: Polygon -> [Edge] -> Affine
turnToFit p eds = let
                es = map (\(E a b) -> (a, b)) . filter isRatLength $ eds
		esn = map (\(a, b) -> (1 / ratRoot ((a - b) `dot` (a - b))) `times` (a - b)) es
		affs = map (\(a, b) -> Affine (0, 0) (a, b) (-b, a)) esn
	in minimumBy' (\x y -> compare (boundSquare $ snd x) (boundSquare $ snd y)) $ map (\af -> (af, map (unapplyAffine af) p)) affs
		where
			minimumBy' f [] = affineId
			minimumBy' f xs = fst $ minimumBy f xs

main = do
	(s, es) <- readStatement <$> getContents
	let cs = convexHull $ concat s
	let
		off = (minimum $ map fst turned, minimum $ map snd turned)
		turn = turnToFit cs (splitEdges es)
		aff = Affine (applyAffine turn off) (aE1 turn) (aE2 turn)
		turned = map (unapplyAffine turn) cs
	let solution = foldConvexCW cs [PolyFold (movePoly aff [(0,0),(0,1),(1,1),(1,0)]) aff]
	putStrLn $ showSolution solution
