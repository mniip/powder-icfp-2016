{-# OPTIONS_GHC -fno-warn-tabs #-}
module Util
	(
		Coord, Pos, Vec, Edge(..), Polygon,
		parallel, intersection, online, inRange, inRange', mirror,
		collinear, overlap,
		dot, cross, times, clockwiseVec, clockwise, convexHull, sortCCW,
		pairs, massCenter, isRatLength, ratRoot, ratRotate, ratRefl,
		Affine(..), mirrorAffine, applyAffine, unapplyAffine, affineId, invertAffine,
		movePoly, unmovePoly, isConvex, splitEdges, polyArea, edgeInSquare, polyIntersect, inPolygonS,
		repeatR, readStatement
	)
	where

import Data.Function
import Data.List
import Data.Monoid
import Data.Ratio
import Data.Tuple
import Data.Typeable
import Data.Maybe (fromJust)

type Coord = Rational

type Pos = (Coord, Coord)

type Vec = Pos

data Edge = E { efst :: Pos, esnd :: Pos } deriving (Show, Read, Typeable)

instance Eq Edge where
	E a b == E c d = (a == c && b == d) || (a == d && b == c)

instance Ord Edge where
	E a b `compare` E c d = case compare a b of
		GT -> case compare c d of
			GT -> (a `compare` c) <> (b `compare` d)
			_ -> (a `compare` d) <> (b `compare` c)
		_ -> case compare c d of
			GT -> (b `compare` c) <> (a `compare` d)
			_ -> (b `compare` d) <> (a `compare` c)

parallel :: Edge -> Edge -> Bool
parallel (E a b) (E c d) = (b - a) `cross` (c - d) == 0

-- Line intersection. Not neccessarily on the provided segments
intersection :: Edge -> Edge -> Maybe Pos
intersection e1@(E a b) e2@(E c d) = if parallel e1 e2
	then if (c - a) `cross` (b - a) == 0
		then Nothing -- collinear
		else Nothing -- parallel
	else Just $ ((c - a) `cross` (b - a)) / ((b - a) `cross` (d - c)) `times` (d - c) + c

collinear :: Edge -> Edge -> Bool
collinear e1@(E a b) e2@(E c d) = parallel e1 e2 && (c - a) `cross` (b - a) == 0

overlap :: Edge -> Edge -> Bool
overlap e1@(E a b) e2@(E c d) = inRange' e1 c || inRange' e1 d || inRange' e2 a || inRange' e2 b

online :: Edge -> Pos -> Bool
online (E a b) c = (c - a) `cross` (c - b) == 0

-- check if a point that is presumably on-line is within a section
inRange :: Edge -> Pos -> Bool
inRange (E (x1, y1) (x2, y2)) (x, y) = between x1 x2 x && between y1 y2 y
	where
		between a b c = case compare a b of
			GT -> a >= c && c >= b
			_ -> b >= c && c >= a

inRange' :: Edge -> Pos -> Bool
inRange' (E (x1, y1) (x2, y2)) (x, y) = between x1 x2 x && between y1 y2 y
	where
		between a b c = case compare a b of
			GT -> a > c && c > b
			EQ -> True
			LT -> b > c && c > a

mirror :: Edge -> Pos -> Pos
mirror (E a b) c = 2 * a + (2 * ((b - a) `dot` (c - a)) / ((b - a) `dot` (b - a))) `times` (b - a) - c

instance (Num a, Num b) => Num (a, b) where
	(a, b) + (c, d) = (a + c, b + d)
	(a, b) - (c, d) = (a - c, b - d)
	(a, b) * (c, d) = (a * c, b * d)
	negate (a, b) = (negate a, negate b)
	abs (a, b) = (abs a, abs b)
	signum (a, b) = (signum a, signum b)
	fromInteger a = (fromInteger a, fromInteger a)

dot :: Vec -> Vec -> Coord
dot (a, b) (c, d) = a * c + b * d
infixl 7 `dot`

cross :: Vec -> Vec -> Coord
cross (a, b) (c, d) = a * d - b * c
infixl 7 `cross`

times :: Coord -> Vec -> Vec
times a (b, c) = (a * b, a * c)
infixl 7 `times`

-- | Is the rotation from a to b clockwise
clockwiseVec :: Vec -> Vec -> Bool
clockwiseVec a b = a `cross` b <= 0

-- | Is the rotation from OA to OB clockwise
clockwise :: Pos -> Pos -> Pos -> Bool
clockwise o a b = clockwiseVec (a - o) (b - o)

-- | CW convex hull.
-- Monotone chain algorithm
convexHull :: [Pos] -> [Pos]
convexHull ps@(_:_:_) = chain (sortBy (flip compare) ps) ++ chain (sortBy compare ps)
	where
		chain = go []
		go aw@(a1:aw1@(a2:as)) xw@(x:xs) = if clockwise a2 a1 x
			then go aw1 xw
			else go (x:aw) xs
		go as (x:xs) = go (x:as) xs
		go as [] = tail as
convexHull ps = ps

sortCCW :: [Pos] -> [Pos]
sortCCW ps = sortBy (\p1 p2 -> if clockwise mid p1 p2 then GT else LT) ps
           where mid = massCenter ps

pairs :: [a] -> [(a, a)]
pairs xs@(_:_) = [(x, y) | x:y:_ <- tails (xs ++ [head xs])]
pairs [] = []

massCenter :: [Pos] -> Pos
massCenter ps = (1 / fromIntegral (length ps)) `times` sum ps

type Polygon = [Pos]

-- Vertexes are supposed to go in counterclockwise order
isConvex :: Polygon -> Bool
isConvex = all (uncurry clock) . pairs . pairs
         where clock (s1, e1) (s2, e2) = (e1-s1) `cross` (e2-s2) >= 0

-- In the convex one
inPolygon :: Polygon -> Pos -> Bool
inPolygon pg p = any (\(a, b) -> inRange (E a b) p) ppg || (all (\(p1, p2) -> clockwise p p2 p1) $ ppg)
               where ppg = pairs pg

-- A strict version, i.e. an "open" polygon
inPolygonS :: Polygon -> Pos -> Bool
inPolygonS pg p = not (any (\(a, b) -> inRange (E a b) p) ppg) && (all (\(p1, p2) -> clockwise p p2 p1) $ ppg)
                where ppg = pairs pg

-- Convex, CCW
polyIntersect :: Polygon -> Polygon -> Bool
polyIntersect p1 p2 = not (any checkAxis (pairs p1))
                     where checkAxis (a, b) = all (clockwise a b) p2

-- Positive if CCW and negative if CW
polyArea :: Polygon -> Coord
polyArea p = 1 % 2 * (sum . map (\(a, b) -> a `cross` b) $ pairs p)

data Affine = Affine { aOrig :: Pos, aE1 :: Pos, aE2 :: Pos } deriving (Show, Read, Eq, Ord, Typeable)

affineId :: Affine
affineId = Affine (0, 0) (1, 0) (0, 1)

mirrorAffine :: Edge -> Affine -> Affine 
mirrorAffine e@(E a b) (Affine o e1 e2) = Affine (mirror e o) (mirror e (e1 + a) - a) (mirror e (e2 + a) - a)

applyAffine :: Affine -> Pos -> Pos
applyAffine (Affine o e1 e2) (x, y) = o + x `times` e1 + y `times` e2

unapplyAffine :: Affine -> Pos -> Pos
unapplyAffine (Affine o e1@(a, b) e2@(c, d)) p = let (x, y) = p - o in (1 / (e1 `cross` e2)) `times` (d * x - c * y, a * y - b * x)

movePoly :: Affine -> [Pos] -> [Pos]
movePoly a = map (applyAffine a)

unmovePoly :: Affine -> [Pos] -> [Pos]
unmovePoly a = map (unapplyAffine a)

invertAffine :: Affine -> Affine
invertAffine (Affine (x, y) e1@(a, b) e2@(c, d)) = Affine (-a*x-b*y, -c*x-d*y) (a, c) (b, d)

isPerfectSquare :: Integer -> Maybe Integer
isPerfectSquare n = binary 0 n
                  where binary low high = let mid = (low + high) `div` 2
                                              mm = mid*mid
                                          in if low > high then Nothing
                                                           else if n == mm then Just mid
                                                                else if n < mm then binary low (mid - 1) else binary (mid + 1) high

ratRoot :: Coord -> Coord
ratRoot c = let n = isPerfectSquare (numerator c)
                d = isPerfectSquare (denominator c)
            in if n /= Nothing && d /= Nothing then fromJust n % fromJust d else undefined -- BOOM

isRatLength :: Edge -> Bool
isRatLength (E a b) = let lsq = abs ((a - b) `dot` (a - b))
                      in isPerfectSquare (numerator lsq) /= Nothing && isPerfectSquare (denominator lsq) /= Nothing

-- The two vectors assumed to be of equal length
ratRotate :: Vec -> Vec -> Affine
ratRotate a@(x1, y1) (x2, y2) = let
		r2 = a `dot` a 
		ca = (y2 * y1 + x2 * x1) / r2
		sa = (y2 * x1 - x2 * y1) / r2
	in Affine (0, 0) (ca, sa) (-sa, ca)

-- ditto
ratRefl :: Vec -> Vec -> Affine
ratRefl a@(x1, y1) (x2, y2) = let
		r2 = a `dot` a 
		ca = (x2 * x1 - y2 * y1) / r2
		sa = (y2 * x1 + x2 * y1) / r2
	in Affine (0, 0) (ca, sa) (sa, -ca)

edgeInSquare :: Edge -> Bool
edgeInSquare (E a b) = insq a && insq b
                     where insq (x, y) = 0 <= x && x <= 1 && 0 <= y && y <= 1

-- TODO: improve
splitEdges :: [Edge] -> [Edge]
splitEdges = findX -- . splitEdges'
	where
		findX es = case [(a, b, c, filter (/= a) $ filter (/= b) es) | a <- es, b <- es, a /= b, not (parallel a b), let Just c = intersection a b, inRange' a c, inRange' b c] of
			[] -> findY es
			(E a b, E c d, p, es):_ -> findX (E a p:E p b:E c p:E p d:es)
		findY es = case [(a, b, filter (/= a) es) | a <- es, E b' b'' <- es, b <- [b', b''], online a b, inRange' a b] of
			[] -> es
			(E a b, p, es):_ -> findY (E a p:E p b:es)

splitEdges' :: [Edge] -> [Edge]
splitEdges' es = foldr (\p -> concatMap (splitOver p)) es points
               where points = nub $ concatMap (\e -> [efst e, esnd e]) es
                     splitOver p edg = if (efst edg /= p) && (esnd edg /= p) && inRange' edg p then [E (efst edg) p, E p (esnd edg)] else [edg]

repeatR :: Int -> (String -> (a, String)) -> String -> ([a], String)
repeatR 0 _ s = ([], s)
repeatR n f s = case f s of
	(a, rs) -> case repeatR (n - 1) f rs of
		(as, rs) -> (a:as, rs)

readSilhouette :: String -> ([Polygon], String)
readSilhouette s = case reads s of
	(i, rs):_ -> repeatR i readPolygon rs
	where
		readPolygon s = case reads s of
			(i, rs):_ -> repeatR i readPos rs
		readPos s = case readFrac s of
			(x, ',':rs) -> case readFrac rs of
				(y, rs) -> ((x, y), rs)
		readFrac s = case reads s of
			(n, '/':rs):_ -> case reads rs of
				(d, rs):_ -> (n % d, rs)
			(n, rs):_ -> (n % 1, rs)

readSkeleton :: String -> [Edge]
readSkeleton s = case reads s of
	(i, rs):_ -> fst $ repeatR i readEdge rs
	where
		readEdge s = let ([st, en], s') = repeatR 2 readPos s
                             in (E st en, s')
		readPos s = case readFrac s of
			(x, ',':rs) -> case readFrac rs of
				(y, rs) -> ((x, y), rs)
		readFrac s = case reads s of
			(n, '/':rs):_ -> case reads rs of
				(d, rs):_ -> (n % d, rs)
			(n, rs):_ -> (n % 1, rs)

readStatement :: String -> ([Polygon], [Edge])
readStatement s = let (p, s') = readSilhouette s
                  in (p, readSkeleton s')

