{-# LANGUAGE BangPatterns #-}
{-# OPTIONS_GHC -fno-warn-tabs #-}
import Control.Applicative
import Control.Arrow
import Control.Concurrent
import Control.Exception
import Control.Monad
import Data.List
import qualified Data.Map.Strict as M
import Data.Maybe
import Data.Monoid
import Data.Ratio
import qualified Data.Set as S
import Data.Tuple
import Data.Function (on)
import Debug.Trace
import Util

import Graphics.Gloss
import System.IO.Unsafe
import Data.IORef

-- Each edge in the skeleton is assinged a color and a direction
type EdgeIdx = Int

-- clockwise
type Piece = [(Bool, EdgeIdx)] -- True = in the same direction as the edge, False = in the opposite direction

data Input = Input {
		origPos :: M.Map EdgeIdx Edge, -- original positions of edges in the skeleton
		edges :: M.Map EdgeIdx Vec, -- edges as vectors
		adjacents :: M.Map EdgeIdx [Piece] -- for every edge, 1 or 2 facets that touch it
	} deriving (Show, Read)

data FoldEdge = FoldEdge {
		idx :: EdgeIdx,
		edge :: Edge, -- where in the square it is located
		cw :: Bool -- True = the free area is CW from the direction
	} deriving (Show, Read)

instance Eq FoldEdge where
	FoldEdge{idx = idx1, edge = E a b} == FoldEdge{idx = idx2, edge = E c d} = idx1 == idx2 && a == c && b == d

instance Ord FoldEdge where
	FoldEdge{idx = idx1, edge = E a b} `compare` FoldEdge{idx = idx2, edge = E c d} = idx1 `compare` idx2 <> a `compare` c <> b `compare` d

data Unfolding = Unfolding {
		freeEdges :: !(S.Set FoldEdge),
		usedEdges :: !(S.Set FoldEdge),
		allFacets :: ![Polygon]
	} deriving (Show, Read)

newtype IOList a = IOList (IORef (Maybe (a, IOList a)))
ioList :: IOList a -> IO [a]
ioList (IOList r) = unsafeInterleaveIO $ do
	v <- readIORef r
	case v of
		Nothing -> ioList (IOList r)
		Just (e, r) -> (e:) <$> ioList r

hist :: IOList Unfolding
hist = unsafePerformIO $ IOList <$> newIORef Nothing

histtail :: IORef (IOList Unfolding)
histtail = unsafePerformIO $ newIORef hist

tailAdd :: IORef (IOList a) -> a -> IO ()
tailAdd v x = do
	IOList old <- readIORef v
	i <- IOList <$> newIORef Nothing
	modifyIORef old $ \Nothing -> Just (x, i)
	writeIORef v i

unfold :: Input -> Unfolding -> [Unfolding]
--unfold i u | unsafePerformIO $ do tailAdd histtail u; return False = undefined
unfold !i !u = do
	let
		pick = last . sortBy (compare `on` interestingEdge) $ S.toList $ freeEdges u
		polys = adjacents i M.! idx pick
	poly' <- polys
	let (dir, hole):poly = cycleUntil (\(_, e) -> e == idx pick) poly'
	let E p p' = edge pick
	let
		(newEdges, newFacet') = unroll ((if cw pick /= dir then ratRefl else ratRotate) (edges i M.! hole) (p' - p)) (cw pick /= dir) (if dir then p' else p) poly
                newFacet = sortCCW newFacet'
		newFacetPoints = newFacet \\ [p, p'] -- don't check old points for intersection
	let (border, inner) = partition (\FoldEdge{edge=E a b} -> any (\s -> online s a && online s b) sides) newEdges
	let newSet = S.fromList inner
	let inter = freeEdges u `S.intersection` newSet
	let ru = Unfolding{
		freeEdges = (S.delete pick (freeEdges u) `S.union` newSet) `S.difference` inter,
		usedEdges = S.insert pick (usedEdges u) `S.union` inter `S.union` S.fromList border,
		allFacets = newFacet : allFacets u
	}
	guard $ all (edgeInSquare . edge) inner -- all edges in square
	guard (not $ or [online (edge edg) point && inRange' (edge edg) point | edg <- S.toList (freeEdges u), point <- newFacetPoints])
	guard $ null [() | FoldEdge{idx=i1,edge=e1} <- inner, FoldEdge{idx=i2,edge=e2} <- S.toList (freeEdges u), i1 /= i2 && e1 == e2]
	guard (not $ or [polyIntersect newFacet facet | facet <- allFacets u]) -- check facets intersection
	--unsafePerformIO $ do tailAdd histtail ru; return [()]
	return ru
	where
		sides = [E (0,0) (1,0), E (1,0) (1,1), E (1,1) (0,1), E (0,1) (0,0)]
                edgesAlign FoldEdge{edge=E a b} = any (\e -> online e a || online e b)
		interestingEdge es = let olde = (map edge . S.toList $ usedEdges u)
                                     in if edgesAlign es sides then 2 
                                         else if edgesAlign es olde then 1 else 0
                cycleUntil :: (a -> Bool) -> [a] -> [a]
		cycleUntil p = go []
			where
				go rs [] = reverse rs
				go rs xw@(x:xs)
					| p x = xw ++ reverse rs
					| otherwise = go (x:rs) xs
		unroll :: Affine -> Bool -> Pos -> Piece -> ([FoldEdge], [Pos])
		unroll a _ cur [] = ([], [cur])
		unroll a m cur ((True, e):es) = let next = cur + applyAffine a (edges i M.! e)
			in case unroll a m next es of
				(fes, ps) -> (FoldEdge{idx = e, edge = E cur next, cw = m} : fes, cur:ps)
		unroll a m cur ((False, e):es) = let next = cur - applyAffine a (edges i M.! e)
			in case unroll a m next es of
				(fes, ps) -> (FoldEdge{idx = e, edge = E next cur, cw = not m} : fes, cur:ps)

type EGraph = M.Map Pos [((Bool, EdgeIdx), Pos)]

buildGraph :: [(EdgeIdx, Edge)] -> [Pos] -> EGraph
buildGraph esl ps = M.fromList $ map (\p -> (p, adj p)) ps
                 where adj p = from ++ to
                             where from = map (\(k, E a b) -> ((True, k), b)) $ filter (\(_, E a b) -> a == p) esl
                                   to = map (\(k, E a b) -> ((False, k), a)) $ filter (\(_, E a b) -> b == p) esl

-- Gives _all_ facets adjacent to an edge (so may contain some facets lying in others)
facetsThroughEdge :: M.Map EdgeIdx Edge -> EGraph -> EdgeIdx -> [Piece]
facetsThroughEdge es eg i = let E a b = es M.! i
                            in map fst . reverse $ sortBy (compare `on` snd) (go a b [a] [(False, i)] ++ go b a [b] [(True, i)])
                            where checkPoly ps = let len = length ps
                                                     [p1, p2, p3, p4] = (drop (len - 2) ps) ++ (take 2 ps)
                                                 in True -- ((p2 - p1) `cross` (p3 - p1)) /= 0 && ((p2 - p4) `cross` (p3 - p4)) /= 0
                                  go start end vert edgs | start == end && length vert > 2 = if checkPoly vert then [(reverse edgs, polyArea vert)] else []
                                                         | otherwise = let dests = filter (\(_, p) -> not (p `elem` vert) && isConvex (end:p:vert)) $ eg M.! start
                                                                       in concatMap (\(e, p) -> go p end (p:vert) (e:edgs)) dests

findFacets :: M.Map EdgeIdx Edge -> EGraph -> EdgeIdx -> [Piece]
findFacets es eg i = let E a b = es M.! i in filter isCW $ go a b [a] [(False, i)] ++ go b a [b] [(True, i)]
	where
--		go start end vert edgs | trace ("start="++show start++" end="++show end++" vert="++show vert++" edgs="++show edgs) False = undefined
		go start end vert edgs
			| start == end = if length vert > 2 then [reverse edgs] else []
			| otherwise = let
					pts = filter (\(e, p) -> not (p `elem` vert) && snd e /= snd (head edgs)) $ eg M.! start
					dests = if null pts then [] else [maximumBy (cwestTo start $ head edgs) pts]
				in {-trace ("Found dests="++show dests) $-} dests >>= \(e, p) -> go p end (p:vert) (e:edgs)
		cwestTo mid (d, targ) xw@(_, x) yw@(_, y) = let
				dx = x - mid
				dy = y - mid
				dt = case es M.! targ of E a b -> if d then b - a else a - b
			in case (clockwiseVec dt dx, clockwiseVec dt dy) of
				(False, True) -> LT
				(True, False) -> GT
				(True, True) -> if clockwiseVec dx dy then LT else GT
				(False, False) -> if clockwiseVec dx dy then LT else GT
		isCW ps = polyArea (go ps (0, 0)) < 0
			where
				go ((dir, i):ps) cur = let E a b = es M.! i in cur:go ps (cur + if dir then b - a else a - b) 
				go [] _ = []

main = do
	(sil, es') <- readStatement <$> getContents
	let
		es = splitEdges es'
                points = nub $ concatMap (\(E a b) -> [a, b]) es
                origList = zip [0..] es
		orig = M.fromList origList
                egraph = buildGraph origList points
		inp = Input{
			origPos = orig,
			edges = M.map (\(E a b) -> b - a) orig,
			adjacents = let f = facetsThroughEdge orig egraph
                                    in M.fromList . map (\i -> (i, f i)) $ M.keys orig
		}
		solutions = do
			e <- M.keys orig
			guard $ isRatLength (orig M.! e)
			let E a b = orig M.! e
			let len = ratRoot $ (b - a) `dot` (b - a)
			dir <- [False, True]
			let edge = FoldEdge{idx = e, edge = if dir then E (0, 0) (len, 0) else E (len, 0) (0, 0), cw = not dir}
			go inp Unfolding{freeEdges = S.singleton edge, usedEdges = S.empty, allFacets = []}
	let
		fullsols = filter (\s -> let ps = S.fromList $ concatMap (\e -> let E a b = orig M.! idx e in [a, b]) $ S.toList $ usedEdges s in all (`S.member` ps) (concat sil)) solutions
		sol = head fullsols
		--sol = head solutions
		(names, points) = assignNames $ concat $ allFacets sol
		mapping = M.fromList $ concatMap (\FoldEdge{idx=i, edge=E a b} -> let E c d = orig M.! i in [(a, c), (b, d)]) $ S.toList $ usedEdges sol
		result = show (M.size points) ++ "\n"
			++ intercalate "\n" (map (showPos . snd) $ M.toAscList points) ++ "\n"
			++ show (length $ allFacets sol) ++ "\n"
			++ intercalate "\n" (map showFacet $ allFacets sol) ++ "\n"
			++ intercalate "\n" (map (showPos . (mapping M.!) . snd) $ M.toAscList points)
		showFacet ps = show (length ps) ++ " " ++ intercalate " " (map show $ map (names M.!) ps)
		showPos (x,y) = showFrac x ++ "," ++ showFrac y
		showFrac a = if denominator a == 1
			then show (numerator a)
			else show (numerator a) ++ "/" ++ show (denominator a)
	mainTh <- myThreadId
	forkIO (do threadDelay 10000000; killThread mainTh)
	putStrLn result
	--test orig solutions
	--test orig [sol]
	where
		go :: Input -> Unfolding -> [Unfolding]
		go inp u
			| S.null $ freeEdges u = return u
--			| length hist > 0 = return (reverse hist, usedEdges u)
			| otherwise = unfold inp u >>= go inp
		assignNames :: Ord a => [a] -> (M.Map a Int, M.Map Int a)
		assignNames = go 0 M.empty M.empty
			where
				go !n !m !im [] = (m, im)
				go n m im (x:xs) = if M.member x m
					then go n m im xs
					else go (n + 1) (M.insert x n m) (M.insert n x im) xs

test :: M.Map EdgeIdx Edge -> [Unfolding] -> IO ()
test es us = animate (InWindow "" (900, 600) (0, 0)) white $ \t -> let u = us !! (round (t * 50)) in Scale 300 300 $ Translate (-1) (-0.5) $ Pictures [drawUnfolding u, Translate 1.1 0 $ drawSkeleton es u, drawFacets u]
	where
		drawUnfolding u = Pictures $ map drawFree (S.toList $ freeEdges u) ++ map drawUsed (S.toList $ usedEdges u)
		drawFree FoldEdge{edge=E a@(x1,y1) b@(x2,y2),cw=cw} = Color red $ Line [(fromRational x1, fromRational y1), (fromRational x2, fromRational y2), (fromRational x, fromRational y)]
			where (x, y) = 1/8 `times` swap ((if cw then (-1, 1) else (1, -1)) * (b - a)) - 1/4 `times` (b - a) + b
		drawUsed FoldEdge{idx=i,edge=E(x1,y1)(x2,y2)} = Pictures[Line [(fromRational x1, fromRational y1), (fromRational x2, fromRational y2)], Translate (fromRational (x1 + x2) / 2) (fromRational (y1 + y2) / 2) $ Scale 0.0003 0.0003 $ Text $ show i]
		drawSkeleton es u = Color green $ Pictures $ concatMap (\(i, E (x1,y1) (x2,y2)) -> [Line [(fromRational x1, fromRational y1), (fromRational x2, fromRational y2)], Translate (fromRational (x1 + x2) / 2) (fromRational (y1 + y2) / 2) $ Scale 0.0003 0.0003 $ Text $ show i]) (M.toList es) ++ [Color (withAlpha' 0.2 green) $ Pictures $ map (\ps -> Polygon $ map (fromRational *** fromRational) $ map (mapping u M.!) ps) $ allFacets u]
		drawFacets u = Color (withAlpha' 0.4 red) $ Pictures $ map (\ps -> Polygon $ map (fromRational *** fromRational) ps) $ allFacets u
		mapping u = M.fromList $ concatMap (\FoldEdge{idx=i, edge=E a b} -> let E c d = es M.! i in [(a, c), (b, d)]) $ S.toList (usedEdges u) ++ S.toList (freeEdges u)

withAlpha' a c = let  (r, g, b, _) = rgbaOfColor c in   makeColor r g b a
